package com.hw.db.controllers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Forum;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

class forumControllerTest {
    private User loggedIn;
    private Forum toCreate;
    private forumController forumController;

    @BeforeEach
    void setUp() {
        loggedIn = new User("some", "some@email.mu", "name", "nothing");
        toCreate = new Forum(12, "some", 3, "title", "some");
        forumController = new forumController();
    }

    @Test
    @DisplayName("Test creating a forum successfully")
    void testCreatingForumSuccessfully() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some")).thenReturn(loggedIn);

            try (MockedStatic<ForumDAO> forumMock = Mockito.mockStatic(ForumDAO.class)) {
                ResponseEntity<Forum> response = forumController.create(toCreate);

                assertEquals(HttpStatus.CREATED, response.getStatusCode(), "Expected status code to be CREATED");
                assertEquals(toCreate, response.getBody(), "Expected the response body to match the created forum");
            }

            assertEquals(loggedIn, UserDAO.Search("some"));
        }
    }
}